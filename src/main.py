from .weather import Weather
from .watchmanager import InfiniTimeDevice
from .config import NOTIFICATION

import dbus
import datetime
import logging

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


def main(version):
    """A companion service script for InfiniTime

    Args:
        version (str): Script version
    """
    # Init dbus notifications if we want them
    # Set this in `config.py`
    if NOTIFICATION is True:
        notify_interface = dbus.Interface(
            object=dbus.SessionBus().get_object(
                        "org.freedesktop.Notifications",
                        "/org/freedesktop/Notifications"),
            dbus_interface="org.freedesktop.Notifications")

    weather = Weather()

    # Fetch the weather data from OpenWeatherMap
    if weather.update():
        actual, forecast = weather.parse_data_file()
        logger.debug("({})".format(actual))
        png = weather.get_icon(actual["weathericon"])
        for day in forecast:
            logger.debug("({})".format(day))

        if NOTIFICATION is True:
            notify_interface.CloseNotification(33)
            notify_interface.Notify("wttrpy",
                                    33,
                                    png,
                                    str(actual["maintemp"]) + "°",
                                    actual["weatherdescription"],
                                    [],
                                    {"urgency": 1},
                                    10000
                                    )

        # Compose the alert
        notification_value = 0
        message = notification_value.to_bytes(3, byteorder='little') + str(
                "Weather@" + datetime.datetime.now().strftime("%X")
            ).encode('utf_8')

        # Connect and scan watch
        watch = InfiniTimeDevice()
        if watch.device is not None:
            if watch.scan_for_service():
                watch.resolve_services()
                watch.send_data(actual, forecast)
                watch.send_notification(message)
            watch.disconnect()
    else:
        logger.error("Something went wrong :-(")
        logger.info("Did you set the API key and watch mac?")
