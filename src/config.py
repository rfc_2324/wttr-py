# You have to set the following variables in order to use OpenWeatherMap
# See: 'https://openweathermap.org/forecast5' for details

from os import environ

# CITY_ID = "2761369"     # Vienna, AT
CITY_ID = "2760887"     # Wolkersdorf, AT
LAT = "48.3833"         # Wolkersdorf
LONG = "16.5167"        # Wolkersdorf
UNITS = "metric"        # [standard|metric|imperial]
LANG = "en"
# Use one of the following
# API_KEY = "your_api_key"              # From string
API_KEY = environ.get('OWM_API_KEY')    # From env var
# Also one of these
# WATCH_MAC = "your_watch_mac"
WATCH_MAC = environ.get('WATCH_MAC')

# Show notifications
NOTIFICATION = False
