from .config import (
    CITY_ID,
    UNITS,
    LANG,
    API_KEY
)
import xdg.BaseDirectory
import os
import requests
import json
import logging

logger = logging.getLogger(__name__)


class Weather:
    """A class that interacts with the OpenWeatherMap API
    """
    def __init__(self):
        self.data_dir = xdg.BaseDirectory.xdg_data_home + "/wttrpy"
        self.data_file = self.data_dir + "/wttr.json"
        self.url = "http://api.openweathermap.org/data/2.5/forecast"
        self.params = (
            ("id", CITY_ID),
            ("units", UNITS),
            ("lang", LANG),
            ("appid", API_KEY)
        )

    def _make_request(self) -> dict:
        """Request data from printhost

        Args:
            query (url): HTTP request

        Returns:
            dict: The deserialized response or None
        """
        try:
            response = requests.get(self.url, params=self.params, timeout=5.0)
        except requests.ConnectTimeout as e:
            logger.debug("ConnectTimeout: ({})".format(e))
            return None
        except requests.ConnectionError as e:
            logger.debug("ConnectionError: ({})".format(e))
            return None
        except requests.Timeout as e:
            logger.debug("Timeout: ({})".format(e))
            return None
        except requests.exceptions.HTTPError as e:
            logger.debug("HTTPError: ({})".format(e))
            return None

        if response.status_code == requests.codes.ok:
            return self._load_json(response)
        else:
            logger.warning("Status: ({})".format(response.status_code))
            return None

    def _load_json(self, r) -> dict:
        """Parse the json response

        Args:
            r (response): requests.Response

        Returns:
            dict: Deserialized json
        """
        try:
            j = json.loads(r.text)
        except json.JSONDecodeError as e:
            logger.warning("Json error: ({})".format(e.msg))
            return None
        else:
            return j

    def _write_data_file(self, data):
        """Write weather data file

        Args:
            data (dict): The weather data to write
        """
        # Create directory if not exists
        os.makedirs(os.path.dirname(self.data_file), exist_ok=True)
        with open(self.data_file, "wt") as f:
            f.write(json.dumps(data, indent=2))
            logger.debug("Wrote ({})".format(self.data_file))

    def _map_weather_icon(self, icon_code) -> str:
        """Map OWM weather codes to available icons on the watch

        Args:
            icon_code (str): see: https://openweathermap.org/weather-conditions

        Returns:
            str: The code to switch in InfiniTime Weather app
        """
        if icon_code == "01d" or icon_code == "01n":
            return "1"
        if icon_code == "02d" or icon_code == "02n":
            return "2"
        if icon_code == "03d" or icon_code == "03n":
            return "3"
        if icon_code == "04d" or icon_code == "04n":
            return "4"
        if icon_code == "09d" or icon_code == "09n":
            return "9"
        if icon_code == "10d" or icon_code == "10n":
            return "10"
        if icon_code == "11d" or icon_code == "11n":
            return "11"
        if icon_code == "13d" or icon_code == "13n":
            return "13"
        if icon_code == "50d" or icon_code == "50n":
            return "50"

    def update(self) -> bool:
        """Request 5 day weather forecast

        Returns:
            bool: Success
        """
        response = self._make_request()
        if response is not None:
            self._write_data_file(response)
            return True
        else:
            logger.warning("Could not fetch data")
            return False

    def parse_data_file(self):
        """Read the downloaded json file and parse the data

        Returns:
            dict, list: The actual and future weather data
        """
        actual = {
            "day": None,
            "city": None,
            "maintemp": None,
            "mintemp": None,
            "maxtemp": None,
            "mainhumidity": None,
            "mainpop": None,            # probability of precipitation
            "weatherdescription": None,
            "weathericoncode": None,
            "weathericon": None,
            "windspeed": None,
            "winddeg": None
        }
        forecast = list()

        with open(self.data_file, "rt") as f:
            data = json.load(f)

        city = data["city"]["name"]
        first, rest = data["list"][0], data["list"][1:]

        # Actual is the first entry in the list
        # The first entry in weather list is the primary
        actual["day"] = first["dt_txt"]
        actual["city"] = city
        actual["maintemp"] = int(round(first["main"]["temp"]))
        actual["mintemp"] = int(round(first["main"]["temp_min"]))
        actual["maxtemp"] = int(round(first["main"]["temp_max"]))
        actual["mainhumidity"] = int(first["main"]["humidity"])
        actual["mainpop"] = int(round(first["pop"]))
        actual["weatherdescription"] = str(first["weather"][0]["description"])
        actual["weathericon"] = str(first["weather"][0]["icon"])
        actual["weathericoncode"] = int(self._map_weather_icon(
            actual["weathericon"])
            )
        actual["windspeed"] = int(first["wind"]["speed"])
        actual["winddeg"] = int(first["wind"]["deg"])

        # The rest is forecast
        for item in rest:
            # Forecast starts at the next day
            testday = actual.get("day")[:10] != item.get("dt_txt")[:10]
            # Value at midnight
            testmidnight = item.get("dt_txt")[11:13] == "00"
            # Value at noon
            testnoon = item.get("dt_txt")[11:13] == "12"
            if testday and testmidnight:
                mintemp = int(round(item["main"]["temp"]))
            if testday and testnoon:
                forecast.append(
                    {
                        "day": str(item["dt_txt"]),
                        "mintemp": int(mintemp),
                        "maxtemp": int(round(item["main"]["temp"])),
                        "weathericoncode": int(self._map_weather_icon(
                            item["weather"][0]["icon"]
                            ))
                    }
                )

        return actual, forecast

    def get_icon(self, ico) -> str:
        """Get the path to the 'ico' png

        Args:
            ico (str): Weathericon code

        Returns:
            str: The path to the png file or None
        """
        # If we have it already use it
        f = self.data_dir + "/" + ico + ".png"
        if os.path.exists(f):
            return f
        # Only download if not
        url = "http://openweathermap.org/img/wn/" + ico + "@2x.png"
        r = requests.get(url, stream=True)
        if r.status_code == 200:
            r.raw.decode_content = True
            with open(f, "wb") as png:
                for chunk in r:
                    png.write(chunk)
                logger.debug("Downloaded: ({})".format(f))
                return f
        return None
