from .config import WATCH_MAC
import logging
from bluepy.btle import (
    BTLEException,
    Peripheral,
)

logger = logging.getLogger(__name__)

BTSVC_INFO = "0000180a-0000-1000-8000-00805f9b34fb"
BTSVC_ALERT = "00001811-0000-1000-8000-00805f9b34fb"
BTSVC_WEATHER = "00050000-78fc-48fe-8e23-433b3a1942d0"
BTCHAR_MANUFACTURER = "00002a29-0000-1000-8000-00805f9b34fb"
BTCHAR_FIRMWARE = "00002a26-0000-1000-8000-00805f9b34fb"
BTCHAR_NEWALERT = "00002a46-0000-1000-8000-00805f9b34fb"
BTCHAR_LOCATION = "00050001-78fc-48fe-8e23-433b3a1942d0"
BTCHAR_CONDITION = "00050002-78fc-48fe-8e23-433b3a1942d0"
BTCHAR_TIMESTAMP = "00050003-78fc-48fe-8e23-433b3a1942d0"
BTCHAR_TZ_OFFSET = "00050004-78fc-48fe-8e23-433b3a1942d0"
BTCHAR_CURRENT = "00050005-78fc-48fe-8e23-433b3a1942d0"
BTCHAR_FORECAST1 = "00050006-78fc-48fe-8e23-433b3a1942d0"
BTCHAR_FORECAST2 = "00050007-78fc-48fe-8e23-433b3a1942d0"
BTCHAR_FORECAST3 = "00050008-78fc-48fe-8e23-433b3a1942d0"


class InfiniTimeDevice():
    """Class that represents an InfiniTime watch

    Args:
        GObject (GObject): Needed for signals
    """

    def __init__(self):
        self.device = None
        self.mac = WATCH_MAC
        self.device_set = set()
        self.aliases = dict()
        self.can_write = False

        self.init()

    def init(self) -> Peripheral:
        """Initialize watch

        Returns:
            Peripheral: A device object or None
        """
        try:
            self.device = Peripheral(self.mac, "random")
        except (BTLEException, ValueError) as e:
            logger.debug("Watch init failed ({})".format(e))
            logger.debug("Maybe run: 'echo \"power on\" | bluetoothctl'")
            return None
        logger.info("Watch state: ({})".format(self.device.getState()))
        return self.device

    def addr(self) -> str:
        """Return the mac address

        Returns:
            str: The device object mac address
        """
        return self.device.addr

    def disconnect(self):
        """Disconnect the device
        """
        self.device.disconnect()

    def scan_for_service(self) -> bool:
        """Scan for weatherservice
        """
        logger.debug("Scanning %s" % (self.device.addr))
        try:
            wservice = self.device.getServiceByUUID(BTSVC_WEATHER)
        except BTLEException as e:
            logger.error("Weatherservice: ({})".format(e))
            return False

        logger.info("Weatherservice: ({})".format(wservice.uuid))
        return True

    def resolve_services(self) -> bool:
        """Map services to handles
        """
        if self.device is None:
            return False

        infosvc = self.device.getServiceByUUID(BTSVC_INFO)
        alertsvc = self.device.getServiceByUUID(BTSVC_ALERT)
        weathersvc = self.device.getServiceByUUID(BTSVC_WEATHER)

        self.manufact = b"n/a"
        self.firmware = b"n/a"
        if infosvc:
            man = infosvc.getCharacteristics(
                BTCHAR_MANUFACTURER
            )
            self.manufact = man[0].read()
            firm = infosvc.getCharacteristics(
                BTCHAR_FIRMWARE
            )
            self.firmware = firm[0].read()

        if alertsvc:
            for c in alertsvc.getCharacteristics():
                if c.uuid == BTCHAR_NEWALERT:
                    self.alert_handle = c.getHandle()

        if weathersvc:
            self.can_write = True
            for c in weathersvc.getCharacteristics():
                logger.info("c: ({})".format(c.uuid))
                if c.uuid == BTCHAR_LOCATION:
                    self.location_handle = c.getHandle()
                elif c.uuid == BTCHAR_CONDITION:
                    self.condition_handle = c.getHandle()
                elif c.uuid == BTCHAR_TIMESTAMP:
                    self.timestamp_handle = c.getHandle()
                elif c.uuid == BTCHAR_TZ_OFFSET:
                    self.tz_offset_handle = c.getHandle()
                elif c.uuid == BTCHAR_CURRENT:
                    self.current_handle = c.getHandle()
                elif c.uuid == BTCHAR_FORECAST1:
                    self.forecast1_handle = c.getHandle()
                elif c.uuid == BTCHAR_FORECAST2:
                    self.forecast2_handle = c.getHandle()
                elif c.uuid == BTCHAR_FORECAST3:
                    self.forecast3_handle = c.getHandle()

        return True

    def send_data(self, actual, forecast):
        """This sends the data via bluetooth

        Args:
            data_dict (dict): Data to send defined in network.py
        """
        self.can_write = False
        logger.debug("watch can_write False")

        # 00050001-78fc-48fe-8e23-433b3a1942d0
        dat = str(actual["city"]).encode('utf_8')
        logger.debug("dat: ({}): ".format(dat))
        self.device.writeCharacteristic(self.location_handle, dat)
        # 00050002-78fc-48fe-8e23-433b3a1942d0
        dat = str(actual["weatherdescription"]).encode('utf_8')
        logger.debug("dat: ({}): ".format(dat))
        self.device.writeCharacteristic(self.condition_handle, dat)
        # 00050003-78fc-48fe-8e23-433b3a1942d0
        dat = str(actual["day"]).encode('utf_8')
        logger.debug("dat: ({}): ".format(dat))
        self.device.writeCharacteristic(self.timestamp_handle, dat)
        # 00050004-78fc-48fe-8e23-433b3a1942d0
        dat = "0".encode('utf_8')
        logger.debug("dat: ({}): ".format(dat))
        self.device.writeCharacteristic(self.tz_offset_handle, dat)
        # "00050005-78fc-48fe-8e23-433b3a1942d0"
        dat = (str(actual["weathericoncode"]) + ':').encode('utf_8') + \
            (str(actual["maintemp"]) + ':').encode('utf_8') + \
            (str(actual["mainhumidity"]) + ':').encode('utf_8') + \
            (str(actual["maxtemp"]) + ':').encode('utf_8') + \
            (str(actual["mintemp"]) + '\0').encode('utf_8')
        logger.debug("dat: ({}): ".format(dat))
        self.device.writeCharacteristic(self.current_handle, dat)
        # "00050006-78fc-48fe-8e23-433b3a1942d0"
        dat = (str(forecast[0]["weathericoncode"]) + ':').encode('utf_8') + \
            (str(forecast[0]["maxtemp"]) + ':').encode('utf_8') + \
            (str(forecast[0]["mintemp"]) + '\0').encode('utf_8')
        logger.debug("dat: ({}): ".format(dat))
        self.device.writeCharacteristic(self.forecast1_handle, dat)
        # "00050007-78fc-48fe-8e23-433b3a1942d0"
        dat = (str(forecast[1]["weathericoncode"]) + ':').encode('utf_8') + \
            (str(forecast[1]["maxtemp"]) + ':').encode('utf_8') + \
            (str(forecast[1]["mintemp"]) + '\0').encode('utf_8')
        logger.debug("dat: ({}): ".format(dat))
        self.device.writeCharacteristic(self.forecast2_handle, dat)
        # "00050008-78fc-48fe-8e23-433b3a1942d0"
        dat = (str(forecast[2]["weathericoncode"]) + ':').encode('utf_8') + \
            (str(forecast[2]["maxtemp"]) + ':').encode('utf_8') + \
            (str(forecast[2]["mintemp"]) + '\0').encode('utf_8')
        logger.debug("dat: ({}): ".format(dat))
        self.device.writeCharacteristic(self.forecast3_handle, dat)

        self.can_write = True
        logger.debug("watch can_write True")

    def send_notification(self, msg):
        """Send a notification to the device

        Args:
            msg (str): The encoded message to send
        """
        logger.debug(msg)
        self.device.writeCharacteristic(self.alert_handle, msg)

    def int_to_bytes(self, number: int) -> bytes:
        return number.to_bytes(length=(
            8 + (number + (number < 0)).bit_length()) // 8,
            byteorder='little', signed=True)


class BluetoothDisabled(Exception):
    pass


class NoAdapterFound(Exception):
    pass


class BTLEDisconnectError(Exception):
    logger.debug("Device disconnected")
