# wttr-py

Weather daemon for Linux on Mobile

To create a systemd user service:
>Copy the files in the 'service' folder to: _'$HOME/.config/systemd/user/'_ and
edit _'wttrpy.service'_ as needed.

Enable the service:
```
$ systemctl --user start wttrpy.timer
$ systemctl --user enable wttrpy.timer
```